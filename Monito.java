package org.yourorghere;



import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;



/**
 * Monito.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class Monito extends JFrame implements KeyListener {
    
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;
    
    
    private static float rotarIzq = 0;
    private static float rotarDer = 0;
    private static float rotarcI=0;
    private static float rotarPD=0; 
    private static float rotarPI=0;
                   boolean b=false,c=false;

    
        public Monito() {
        //Propiedades para el frame
        setSize(600, 600);
        setLocationRelativeTo(null);
        setTitle("Movimiento de manos");
        setResizable(false);
        //invocamos a la clase Graphic
        GraphicListener listener = new GraphicListener();
        //Se instancia al canvas y variables
        canvas = new GLCanvas();
        gl = canvas.getGL();
        glu = new GLU();
        glut = new GLUT();

        //Se a�anade el oyente que renderiza los graficos de el metodo
        //display
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);

        //Animaciones
        Animator animator = new Animator(canvas);
        animator.start();

        //A�adimos la interfaz keylistener
        addKeyListener(this);
    }

    //Main principal de la clase
    public static void main(String args[]) {
        Monito frame = new Monito();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    public class GraphicListener implements GLEventListener{

        @Override
        public void init(GLAutoDrawable drawable) {}

        @Override
        public void display(GLAutoDrawable drawable) {
            
            gl = drawable.getGL();
            //limpiamos el buffer
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(.1f, .5f, .9f, 1f);
            //gl.glColor3f(0.5f, 0.5f, 0.5f);
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            
            glu.gluPerspective(50, 2, 0, 30);
            //Establecemos la camara
            glu.gluLookAt(0, 0, 10, 0, 0, 3, 0, 1, 0);
            gl.glLineWidth(5);
            
            
            
        //     Matriz completa
        // <<<<<<<<<<<<<<<=================================>>>>>>>>>>>>>>>>>>>>>
            gl.glPushMatrix();
            
            gl.glRotatef(rotarcI,0,1,0); //   <<<<<<<<<<<<<<<<<===========  angulo total
            
            //Cabeza
            gl.glPushMatrix();
                gl.glTranslatef(0, 1.4f, 1);
                gl.glColor3f(0,0,128);
                glut.glutSolidCube(1.5f);
            gl.glPopMatrix();
            
            // ojos
             gl.glPushMatrix();
                gl.glTranslatef(-.4f, 2f, 1);
                gl.glColor3f(255,255,255);
                glut.glutSolidSphere(.1, 100, 100);
            gl.glPopMatrix();
            
             gl.glPushMatrix();
                gl.glTranslatef(.4f, 2f, 1);
                gl.glColor3f(255,255,255);
                glut.glutSolidSphere(.1, 100, 100);
            gl.glPopMatrix();
            
            
            
            
            //  CUERPO <<<<<<<<<<<<<<<<==============
              gl.glPushMatrix();
                gl.glTranslatef(0f, 0f, 0f);
                gl.glColor3f(0.0f, 0.7656f, 0.5859f);
                glut.glutSolidCube(3);
            gl.glPopMatrix();
            
            //Izquierda
            gl.glPushMatrix();
                 gl.glTranslatef(-2.1f, .1f, 1);
                 gl.glRotatef(rotarIzq, 0 ,0 , 1);
                 gl.glColor3f(192,192,192);
                 glut.glutSolidCube(.9f);
            //   MANO    <<<<<<<<<<<<========================     
                 gl.glTranslatef(.09f, -.6f, 1);
                 gl.glColor3f(250, 0, 250);
                 glut.glutSolidCube(.3f);
            gl.glPopMatrix();  
            
            //     PIE
            gl.glPushMatrix();
                 gl.glTranslatef(-.85f ,-2.37f,1);
                 gl.glRotatef(rotarPI,1,0,0);
                 gl.glColor3f(192,192,192);
                 glut.glutSolidCube(1.25f);
            gl.glPopMatrix();
            
             //*************************************
            //Derecha       <<<<<<<<<<<<<===========
            gl.glPushMatrix();
                gl.glTranslatef(2.1f, .1f, 1);
                gl.glRotatef(rotarDer, 0, 0, 1);
                gl.glColor3f(192, 192, 192);
                glut.glutSolidCube(.9f);
           //    MANOS  <<<<<<<<<<<<<<<<<<<<<<<<<<<     
                gl.glTranslatef(-.09f, -.6f , 1);
                gl.glColor3f(250, 0, 250);
                glut.glutSolidCube(.3f);
            gl.glPopMatrix();  
            
            //  <<<<<<<<<<<<< PIE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            
            gl.glPushMatrix();
                gl.glTranslatef(.85f, -2.37f, 1);
                gl.glRotatef(rotarPD,1,0,0);
                gl.glColor3f(192, 192, 192);
                glut.glutSolidCube(1.25f);
            gl.glPopMatrix();
     //      7 4 9    1 
     
     
            gl.glPopMatrix();
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) { }

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {   }
        
    }
    
    @Override
    public void keyTyped(KeyEvent e) {      }

    @Override
    public void keyPressed(KeyEvent e) {   
        if(e.getKeyCode() == KeyEvent.VK_UP){
            if(rotarIzq>-160)
                rotarIzq--;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            if(rotarIzq<0)
                rotarIzq++;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            if(rotarDer < 160)
                rotarDer++;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            if(rotarDer > 0)
                rotarDer--;
        }
        
        if(e.getKeyCode()==KeyEvent.VK_A )
            rotarcI++;
        
        if(e.getKeyCode()==KeyEvent.VK_S )
            rotarcI--;
//      <<<<<<<<<<<<<<<<==========================  Pies

        if(e.getKeyCode()==KeyEvent.VK_Z ){ 
            
            System.out.println("PD:"+rotarPD+" b:"+b+" c:"+c);
            
            if(rotarPD<60 && !b && !c){
               rotarPD++;
               
               if(rotarPD==58)
                   b=true;                              
            }
            
            if(b && !c){  
                rotarPD--;
                rotarPI++;
                
                if(rotarPD<2 && !c){
                    b=false;
                    c=true;
                }
            }
            
            if( c && !b ){
                rotarPI--;
                rotarPD++;
                
                if(rotarPD==58 && c){
                c=false;
                b=true;
            }
                
          }
        }
        
    }

    @Override
    public void keyReleased(KeyEvent e) {   }
    
 
    
    
    
    
    
}


